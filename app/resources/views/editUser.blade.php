<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Editar Usuário</title>
</head>
<body>
  <!--  Esse nome ['user'] tem que ser igual nome da rota que colocamos exemplo
   /usuario/edit/{user}  tipo se for ali numero, chip, identidade tinha que muda no form também, tem que ser
   tem que ser igual essa foi atualização do 6.0 que antes 5.8 permitia colocar qualquer número. -->
<form action="{{ route ('users.edit',['user' => $user->id]) }}" method="post">
  @csrf
  <!-- uma forma de enganar o laravel vai ser enviado post, mas no oculto vai ser enviado put -->
  @method('PUT')
    <label for="">Nome do Usuário</label>
    <input  type="text" name="name" value="{{$user->name}}">

    <label for="">E-mail do Usuário</label>
    <input  type="email" name="email" value="{{$user->email}}">

    <label for="">Senha do Usuário</label>
    <input  type="password" name="password" value="{{$user->password}}">

    <input type="submit" value="Editar usuário">
  </form>
  </body>
</html>