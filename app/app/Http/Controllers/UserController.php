<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function listUser()
    {
        // $user = new User();
        // $user->name = 'PW codigo';
        // $user->email = 'pwcodigo@google.com.br';
        // $user->password = Hash::make('123');
        // $user->save();
        // echo "<h1> Listagem de nomes </h1>";
        $user = User::where('id', '=', 1)->first();
        // Essa função dd é para debugar 
        //dd($user);
        return view('listUser', [
            'user' => $user
        ]);
    }
}
