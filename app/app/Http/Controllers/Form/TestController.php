<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class TestController extends Controller
{
    public function listAllUsers()
    {
        $users = User::all();

        return view('listAllUsers', [
            'users' => $users
        ]);
    }

    // informei int na frente, uma forma de segurança para limitar que as entradas vindo da url só aceita se for numero inteiro
    //public function listUser(int $user)

    public function listUser(User $user)
    {
        //var_dump($user);
        return view('listUser', [
            'user' => $user
        ]);
    }

    public function formAddUser()
    {
        return view('addUser');
    }

    // Para resgatar as informações usamos o request
    public function storeUser(Request $request)
    {
        // Aqui $user está pegando as informações vindo do request, vindo da url e armazemamdp no $user
        // E tanto que criei  um objeto para receber as informaçoes do $request
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        // Aqui para salvar no banco de dados
        $user->save();

        /* Quero voltar para minha página para listar os usuários
        após cadastrar e então uso comando abaixo
        */
        return redirect()->route('users.listAll');
    }

    /*
       Dentro do () para pegar os dados vindo da url se chama
       injenção de depedência;

       O lado esquerdo  esta recebendo os dados do $user 
        'user' => $user
        esse $user está vindo do user.php o arquivo que tem no laravel.
        esse caminho 
        use App\User;
    */
    public function formEditUser(User $user)
    {
        return view('editUser', [
            'user' => $user
        ]);
    }

    public function edit(User $user, Request $request)
    {
        /* Quer dizer, estou recebendo da função edit o paramento $user ( nesse caso é o id vindo da url)
        e o User $user dependência de injenção do próprio laravel que puxa os dados do model 
        nesse caso ele vai filtrar pelo id. 
        O Request são informação vindo da URL.
        No caso abaixo, estou atribuindo os valores vindo do request(formulario) ao meu usuário no banco de dados.
        
        Vamos validar os dados vindo da url
        usano filter_var  
        */
        $user->name = $request->name;
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $user->email = $request->email;
        }
        if (!empty($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect()->route('users.listAll');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.listAll');
    }
}
